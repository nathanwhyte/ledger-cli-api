table! {
    posts (id) {
        id -> Int4,
        date_created -> Text,
        memo -> Text,
    }
}

table! {
    transactions (id) {
        id -> Int4,
        post_id -> Int4,
        bucket -> Text,
        currency -> Nullable<Text>,
        credit -> Bool,
        amount -> Int4,
        comment -> Nullable<Text>,
    }
}

joinable!(transactions -> posts (post_id));

allow_tables_to_appear_in_same_query!(posts, transactions,);
