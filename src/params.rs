use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct Parameters {
    pub sort_by: String,

    #[serde(default)]
    pub sort_desc: bool,
}
