mod params;

pub mod models;
pub mod schema;

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate rocket_sync_db_pools;

use models::{FullPost, NewPost, NewTransaction, Post, Transaction};
use params::Parameters;
use schema::{posts, transactions};

use diesel::prelude::*;
use rocket::{
    response::{status::Created, Debug},
    serde::json::Json,
};

pub type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

#[database("ledger_cli_api_db")]
struct DbConnection(diesel::PgConnection);

#[post("/ledger-posts", format = "json", data = "<post>")]
async fn new_ledger_post(db: DbConnection, post: Json<NewPost>) -> Result<Created<Json<i32>>> {
    let result: i32 = db
        .run(move |conn| {
            diesel::insert_into(posts::table)
                .values(&*post)
                .returning(posts::id)
                .get_result(conn)
        })
        .await?;

    Ok(Created::new("/ledger-posts").body(Json::from(result)))
}

#[get("/ledger-posts/all")]
async fn get_post_list(db: DbConnection) -> Result<Json<Vec<i32>>> {
    let ids_list: Vec<i32> = db
        .run(move |conn| posts::table.select(posts::id).load(conn))
        .await?;

    Ok(Json(ids_list))
}

#[get("/ledger-posts", format = "json", data = "<params>")]
async fn get_post_by_params(db: DbConnection, params: Json<Parameters>) -> Result<Json<Vec<Post>>> {
    let posts = match params.sort_by.as_str() {
        "date_created" => {
            db.run(move |conn| {
                let select = posts::table.select(posts::all_columns);
                match params.sort_desc {
                    true => select.order(posts::date_created.desc()).load(conn),
                    false => select.order(posts::date_created.asc()).load(conn),
                }
            })
            .await?
        }
        "memo" => {
            db.run(move |conn| {
                let select = posts::table.select(posts::all_columns);
                match params.sort_desc {
                    true => select.order(posts::memo.desc()).load(conn),
                    false => select.order(posts::memo.asc()).load(conn),
                }
            })
            .await?
        }
        _ => db.run(move |conn| posts::table.load(conn)).await?,
    };

    Ok(Json(posts))
}

// TODO grab any transactions with this post's id and put them in result json
#[get("/ledger-posts/<id>")]
async fn get_post_by_id(db: DbConnection, id: i32) -> Option<Json<FullPost>> {
    let result: Post = db
        .run(move |conn| posts::table.find(id).first(conn))
        .await
        .ok()?;

    let post_transactions: Vec<Transaction> = db
        .run(move |conn| {
            transactions::table
                .select(transactions::all_columns)
                .filter(transactions::post_id.eq(result.id))
                .load(conn)
        })
        .await
        .ok()?;

    Some(Json::from(FullPost::new(result, post_transactions)))
}

#[delete("/ledger-posts/<id>")]
async fn delete_post_with_id(db: DbConnection, id: i32) -> Result<Option<()>> {
    let removed = db
        .run(move |conn| {
            diesel::delete(posts::table)
                .filter(posts::id.eq(id))
                .execute(conn)
        })
        .await?;

    // returns `Ok(Some(()))` if removed == 1 (remove query succeeded)
    // returns `Ok(None)` if removed != 1 (remove query failed)
    Ok((removed == 1).then(|| ()))
}

#[post("/ledger-transactions", format = "json", data = "<transaction>")]
async fn new_ledger_transactions(
    db: DbConnection,
    transaction: Json<NewTransaction>,
) -> Result<Created<Json<i32>>> {
    let result: i32 = db
        .run(move |conn| {
            diesel::insert_into(transactions::table)
                .values(&*transaction)
                .returning(transactions::id)
                .get_result(conn)
        })
        .await?;

    Ok(Created::new("/ledger-transactions").body(Json::from(result)))
}

#[get("/")]
fn default() -> &'static str {
    "Hello there! You've reached the default API endpoint."
}

#[launch]
pub fn rocket() -> _ {
    rocket::build().attach(DbConnection::fairing()).mount(
        "/",
        routes![
            new_ledger_post,
            get_post_by_id,
            get_post_by_params,
            get_post_list,
            delete_post_with_id,
            new_ledger_transactions,
            default
        ],
    )
}
