#[rocket::main]
async fn main() -> Result<(), rocket::Error> {
    let _rocket = ledger_cli_api::rocket().launch().await?;

    Ok(())
}
