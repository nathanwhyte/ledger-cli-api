use super::schema::{posts, transactions};

use serde::{self, Deserialize, Serialize};
use std::fmt::{self, Display};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FullPost {
    pub id: i32,
    pub date_created: String,
    pub memo: String,
    pub transactions: Vec<ConvertedTransaction>,
}

impl FullPost {
    pub fn new(post: Post, transactions: Vec<Transaction>) -> FullPost {
        let mut converted = Vec::new();
        transactions
            .iter()
            .for_each(|t| converted.push(t.into_converted()));
        println!("{:?}", transactions);
        FullPost {
            id: post.id,
            date_created: post.date_created,
            memo: post.memo,
            transactions: converted,
        }
    }
}

impl Display for FullPost {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(
            f,
            "post: {}\n{} * {}",
            self.id, self.date_created, self.memo
        )?;
        self.transactions
            .clone()
            .into_iter()
            .for_each(|t| writeln!(f, "{}", t).expect("error writing to formatter"));

        Ok(())
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, Queryable)]
pub struct Post {
    pub id: i32,
    pub date_created: String,
    pub memo: String,
}

impl Display for Post {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}) {} * {}", &self.id, &self.date_created, &self.memo)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, Insertable)]
#[table_name = "posts"]
pub struct NewPost {
    date_created: String,
    memo: String,
}

impl Display for NewPost {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} * {}", &self.date_created, &self.memo)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, Queryable)]
pub struct Transaction {
    pub id: i32,
    pub post_id: i32,
    pub bucket: String,
    pub currency: Option<String>,
    pub credit: bool,
    pub amount: i32,
    pub comment: Option<String>,
}

impl Transaction {
    pub fn into_converted(&self) -> ConvertedTransaction {
        ConvertedTransaction {
            id: self.id,
            post_id: self.post_id,
            bucket: self.bucket.clone(),
            currency: self.currency.clone(),
            credit: self.credit,
            amount: self.amount as f32 / 100_f32,
            comment: self.comment.clone(),
        }
    }
}

impl Display for Transaction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "    {: <45} {} {} {}",
            self.bucket,
            self.amount,
            self.currency.as_ref().unwrap_or(&"".to_string()),
            self.comment.as_ref().unwrap_or(&"".to_string())
        )
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, Queryable)]
pub struct ConvertedTransaction {
    pub id: i32,
    pub post_id: i32,
    pub bucket: String,
    pub currency: Option<String>,
    pub credit: bool,
    pub amount: f32,
    pub comment: Option<String>,
}

impl Display for ConvertedTransaction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "    {: <45} {} {} {}",
            self.bucket,
            self.amount,
            self.currency.as_ref().unwrap_or(&"".to_string()),
            self.comment.as_ref().unwrap_or(&"".to_string())
        )
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, Insertable)]
#[table_name = "transactions"]
pub struct NewTransaction {
    pub post_id: i32,
    pub bucket: String,
    pub credit: bool,
    pub amount: i32,
    pub currency: Option<String>,
    pub comment: Option<String>,
}
