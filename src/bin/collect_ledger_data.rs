mod request;

use ledger_cli_api::models::NewTransaction;

use reqwest::Client;
use std::{
    ffi::OsStr,
    fs::{self, File},
    io::{BufRead, BufReader},
    path::Path,
};

#[tokio::main]
async fn main() {
    let _remove_all = request::test_remove_all(Client::new()).await;
    export_ledger_data().await;
}

// handle collecting ledger files and exporting data to csv
async fn export_ledger_data() {
    // loop through all files in LEDGER_HOME
    let mut current_post_id = 0;
    for file in fs::read_dir(Path::new("/home/natew/Dropbox/Finance/")).unwrap() {
        let file_name_os_str = file.unwrap().path();
        let file_name_str: &str = file_name_os_str.to_str().unwrap();
        // grab supported files to be exported
        match Path::new(file_name_str).extension().and_then(OsStr::to_str) {
            Some("ledger") | Some("dat") => {
                for line in BufReader::new(File::open(file_name_str).unwrap())
                    .lines()
                    .flatten()
                    .filter(|l| !l.starts_with(';'))
                {
                    if line.is_empty() {
                        continue;
                    }

                    let line_again = line.clone();
                    if line_again.chars().next().unwrap().is_numeric() {
                        let mut tokens = line_again.split(' ');
                        let mut new_memo = String::new();
                        tokens.clone().into_iter().skip(2).for_each(|s| {
                            new_memo.push_str(s);
                            new_memo.push(' ');
                        });
                        current_post_id =
                            request::test_post(Client::new(), tokens.next().unwrap(), &new_memo)
                                .await
                                .unwrap()
                    } else {
                        let tokens: Vec<&str> = line
                            .trim()
                            .split(' ')
                            .filter(|t| !t.is_empty())
                            .filter(|t| {
                                t.chars().next().unwrap().is_alphanumeric() || t.starts_with('-')
                            })
                            .collect();
                        let amount = tokens
                            .get(1)
                            .unwrap()
                            .replace('.', "")
                            .parse::<i32>()
                            .unwrap_or_else(|_| panic!("{:?}", tokens.get(1)));
                        let transaction = NewTransaction {
                            post_id: current_post_id,
                            bucket: tokens.get(0).unwrap().to_string(),
                            currency: tokens.get(2).map(|c| c.to_string()),
                            credit: amount > 0,
                            amount,
                            comment: tokens.get(4).map(|c| c.to_string()),
                        };
                        let _new_t =
                            request::test_new_transaction(Client::new(), transaction).await;
                    }
                }
            }
            _ => continue,
        }
    }
}
