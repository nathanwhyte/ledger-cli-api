#![allow(dead_code)]

extern crate ledger_cli_api;

use ledger_cli_api::models::{FullPost, NewTransaction, Post};

use reqwest::{self, Client, Result};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Parameters {
    pub sort_by: String,

    #[serde(default)]
    pub sort_desc: bool,
}

#[tokio::main]
async fn main() -> Result<()> {
    let client = Client::new();

    // test_get_by_params(client.clone()).await?;

    // test_get_list(client.clone()).await?;

    test_get_id(client.clone(), 14).await?;

    // test_remove_id(client.clone(), 1).await?;

    // test_remove_all(client.clone()).await?;

    Ok(())
}

pub async fn test_post(client: Client, date_created: &str, memo: &str) -> Result<i32> {
    let json_body: HashMap<&str, &str> =
        HashMap::from([("date_created", date_created), ("memo", memo)]);

    let post_response = client
        .post("http://127.0.0.1:8000/ledger-posts")
        .json(&json_body)
        .send()
        .await?;
    let response_json = post_response.json::<i32>().await?;

    Ok(response_json)
}

async fn test_get_list(client: Client) -> Result<()> {
    let response = client
        .get("http://127.0.0.1:8000/ledger-posts/all")
        .send()
        .await?;
    let _response_json: Vec<i32> = response.json::<Vec<i32>>().await?;

    // response_json.iter().for_each(|p| println!("{}", p));
    // println!();

    Ok(())
}

async fn test_get_by_params(client: Client) -> Result<()> {
    let json_body = Parameters {
        sort_by: String::from("memo"),
        sort_desc: true,
    };

    let response = client
        .get("http://127.0.0.1:8000/ledger-posts")
        .json(&json_body)
        .send()
        .await?;
    let response_json: Vec<Post> = response.json::<Vec<Post>>().await?;

    response_json.iter().for_each(|p| println!("{:>2}", p));
    println!();

    Ok(())
}

async fn test_get_id(client: Client, id: i32) -> Result<()> {
    let mut path = String::from("http://127.0.0.1:8000/ledger-posts/");
    path.push_str(&id.to_string());
    let response = client.get(path).send().await?;
    let json_response = response.json::<FullPost>().await?;

    println!("{}", json_response);

    Ok(())
}

async fn test_remove_id(client: Client, id: i32) -> Result<()> {
    let mut path = String::from("http://127.0.0.1:8000/ledger-posts/");
    path.push_str(&id.to_string());
    let _response = client.delete(path).send().await?;

    Ok(())
}

pub async fn test_remove_all(client: Client) -> Result<()> {
    let response = client
        .get("http://127.0.0.1:8000/ledger-posts")
        .send()
        .await?;
    let response_json: Vec<i32> = response.json::<Vec<i32>>().await?;

    for id in response_json {
        test_remove_id(client.clone(), id).await?;
    }

    Ok(())
}

pub async fn test_new_transaction(client: Client, transaction: NewTransaction) -> Result<()> {
    let post_response = client
        .post("http://127.0.0.1:8000/ledger-transactions")
        .json(&transaction)
        .send()
        .await?;
    let response_json = post_response.json::<i32>().await?;

    println!();
    println!("{}", response_json);
    println!();

    Ok(())
}
