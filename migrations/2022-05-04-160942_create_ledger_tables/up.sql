-- Your SQL goes here

create table posts (
  id            serial  primary key,
  date_created  text not null,
  memo          text not null
);

create table transactions (
  id                 serial  primary key,
  post_id     serial  not null,
  bucket             text not null,
  currency           text ,
  credit             boolean not null default 'f',
  amount             integer   not null,
  comment            text ,

  foreign key (post_id) references posts (id)
);
